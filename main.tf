module "vpc" {
  source = "./vpc"
  cidr = var.cidr
  name = var.name
  environment = var.environment
  az = var.az
  private_subnets = var.private_subnets
  public_subnets = var.public_subnets
}

module "security-group" {
  source = "./security-groups"
  name = var.name
  environment = var.environment
  vpc_id = module.vpc.vpc_id
  container_port = var.container_port
}

module "ecs" {
  source = "./ecs"
  name = var.name
  environment = var.environment
  
}