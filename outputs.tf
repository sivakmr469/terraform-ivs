output "vpc_id" {
  value = module.vpc.vpc_id
}

output "container_image"{
  value = module.ecs.container_image
}

output "alb_security_group" {
  value = module.security-group.alb_security_group
}

output "ecs_security_group" {
  value = module.security-group.ecs_security_group
}

output "ecs_arn" {
  value = module.ecs.ecs_arn
}

output "public_subnets" {
  value = module.vpc.public_subnets
}

output "private_subnets" {
  value = module.vpc.private_subnets
}