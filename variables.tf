variable "cidr" {
  default = "10.0.0.0/16"
  type = string
}

variable "name" {
  default = "echo-project"
  type = string
}

variable "environment" {
  default = "dev"
  type = string
}

variable "az" {
  type = list
}

variable "public_subnets" {
  type = list
}

variable "private_subnets" {
  type = list
}

variable "container_port" {
  
}