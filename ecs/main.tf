resource "aws_ecs_cluster" "main" {
  name = "${var.name}-cluster"
  tags = {
    Name        = "${var.name}-cluster"
    Environment = var.environment
  }
}

resource "aws_ecr_repository" "main" {
  name                 = "${var.name}"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }
}

resource "aws_ecr_lifecycle_policy" "main" {
  repository = aws_ecr_repository.main.name

  policy = jsonencode({
    rules = [{
      rulePriority = 1
      description  = "keep last 10 images"
      action       = {
        type = "expire"
      }
      selection     = {
        tagStatus   = "any"
        countType   = "imageCountMoreThan"
        countNumber = 10
      }
    }]
  })
}

output "container_image" {
    value = aws_ecr_repository.main.repository_url
}

output "ecs_arn" {
  value = aws_ecs_cluster.main.id
}


