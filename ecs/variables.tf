variable "name" {
  default = "echo-project"
  type = string
}

variable "environment" {
  default = "dev"
  type = string
}